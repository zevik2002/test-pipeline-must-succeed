export interface TokenPayloadInterface {
  _id: number;
  isAdmin: boolean;
  iat?: number;
  email:string;
  name: {
    first: string;
    last: string;
  };
}

export default TokenPayloadInterface;
