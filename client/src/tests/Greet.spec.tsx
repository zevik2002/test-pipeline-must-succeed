import Greet from "./Greet";
import { render, screen } from "@testing-library/react";

describe("Testing Greet component", () => {
  it("render Shula Zaken in the Greet component", () => {
    render(<Greet />);
    const elementWithText = screen.getByText(/Shula Zaken/);
    expect(elementWithText).toBeTruthy();
  });

  it("render David Yakin in the Greet component", () => {
    render(<Greet name="David Yakin" />);
    const elementWithText = screen.getByText(/David Yakin/);
    expect(elementWithText).toBeInTheDocument();
  });
});
//Zevitheking