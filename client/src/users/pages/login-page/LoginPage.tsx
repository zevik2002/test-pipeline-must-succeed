import {
  Button,
  Box,
  Typography,
  Container,
  Grid,
  FormControl,
} from "@mui/material";
import ROUTES from "../../../global/router/routes-model";
import useSendRequestToServer from "../../../posts/hooks/useSendRequestToServer";
import { SubmitHandler, useForm } from "react-hook-form";
import { UserFormInput } from "../../../global/types/types";
import InputFieldsLogIn from "./InputFieldsLogIn";
import Spinner from "../../../global/components/Spinner";
import { useNavigate } from "react-router-dom";
import TextLink from "../../../global/components/textLink";
import { useErrorPage } from "../../../global/hooks/useErrorPage";
import { useAuth } from "../../../auth/AuthProvider";
import { toastHandler } from "../../../global/components/toastify/toastHandler";
import { useEffect } from "react";

const LoginPage = () => {
  const navigate = useNavigate();
  const { pending, error, handleSubmitLogin } = useSendRequestToServer<
    string | null
  >();

  const { user, token } = useAuth();

  useErrorPage(
    error === `Network Error` ? { status: 503, message: error } : null
  );

  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
    trigger,
    reset,
  } = useForm<UserFormInput>();

  const onSubmit: SubmitHandler<UserFormInput> = (
    dataForLogin: UserFormInput
  ) => {
    handleSubmitLogin(dataForLogin);
    reset();
  };

  useEffect(() => {
    if (!token && error && !pending)
      toastHandler("email or password not valid", "error");
  }, [token, error, pending]);

  useEffect(() => {
    if (user) {
      toastHandler("login successful !", "success");
      navigate(ROUTES.HOME);
    }
  }, [user]);
  return (
    <Container component="main" maxWidth="xs">
      <Box
        position="absolute"
        top="50%"
        left="50%"
        sx={{
          width: "100%",
          maxWidth: "400px",
          transform: "translate(-50%, -50%)",
          textAlign: "center",
        }}
      >
        <Typography component="h1" variant="h5">
          Login
        </Typography>
        <FormControl
          fullWidth
          component="form"
          onSubmit={handleSubmit(onSubmit)}
        >
          <InputFieldsLogIn
            register={register}
            formState={{ errors }}
            trigger={trigger}
          />
          {isValid && (
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Login
            </Button>
          )}

          {pending && <Spinner />}

          <Grid container justifyContent="center">
            <TextLink text="Not registered yet" to={ROUTES.SIGNUP} />
          </Grid>
        </FormControl>
      </Box>
    </Container>
  );
};

export default LoginPage;

