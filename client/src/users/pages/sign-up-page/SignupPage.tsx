import { useForm, SubmitHandler } from "react-hook-form";
import { Box, Button, Container, FormControl, Typography } from "@mui/material";
import { UserFormInput } from "../../../global/types/types";
import InputFields from "./InputFields";
import TextLink from "../../../global/components/textLink";
import useSendRequestToServer from "../../../posts/hooks/useSendRequestToServer";
import Spinner from "../../../global/components/Spinner";
import ROUTES from "../../../global/router/routes-model";
import { useErrorPage } from "../../../global/hooks/useErrorPage";
import { useNavigate } from "react-router-dom";
import { toastHandler } from "../../../global/components/toastify/toastHandler";
import { useEffect } from "react";

const SignupPage = () => {
  const {
    register,
    handleSubmit,
    formState: { isValid, errors },
    trigger,
    reset,
  } = useForm<UserFormInput>();
  const { data, error, pending, handelSignUp } = useSendRequestToServer();
  const navigate = useNavigate();

  useErrorPage(error === `Network Error` ? { status: 503, message: error } : null);

  useEffect(() => {
    if (data) {
      toastHandler(`User created successfully!`, `success`);
      navigate(ROUTES.LOGIN);
    } else if (error === "Request failed with status code 401") {
      toastHandler(`Email is already exist !`, `error`);
    } else if (error && error !== `Network Error`) {
      toastHandler(`Error in creating new user`, `error`);
    }
  }, [data, error]);
  

  const onSubmit: SubmitHandler<UserFormInput> = (dataFromInputs: UserFormInput) => {
    handelSignUp(dataFromInputs);
    reset();
  };

  return (
    <Container component="main" maxWidth="xs">
      <Box
        position="absolute"
        top="50%"
        left="50%"
        sx={{
          width: "100%",
          maxWidth: "400px",
          transform: "translate(-50%, -50%)",
          textAlign: "center",
        }}>
        <Typography sx={{ marginBottom: 2 }} component="h1" variant="h5">
          {" "}
          Sign Up
        </Typography>
        <FormControl fullWidth component="form" onSubmit={handleSubmit(onSubmit)}>
          <InputFields register={register} formState={{ errors }} trigger={trigger} />
          {isValid && (
            <Button fullWidth variant="contained" sx={{ mt: 3, mb: 2 }} type="submit">
              Sign up
            </Button>
          )}
          {pending && <Spinner />}
        </FormControl>
        <TextLink text="Allredy register" to={ROUTES.LOGIN} />
      </Box>
    </Container>
  );
};
export default SignupPage;
