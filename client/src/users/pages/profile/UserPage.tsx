import { Typography, Container, Grid } from "@mui/material";
import ROUTES from "../../../global/router/routes-model";
import NavLink from "../../../global/router/components/NavLink";

const UserPage = () => {
  return (
    <Container>
      <Grid
        sx={{ display: "flex", justifyContent: "center", marginTop: "15px" }}
      >
        <Typography variant="h3">User Page</Typography>
      </Grid>
      <Grid container spacing={5}>
        <Grid item xs={12} sm={6}>
          <NavLink
            to={ROUTES.PROFILE}
            sx={{
              textDecoration: "none",
              color: "inherit",
              border: "1px solid white",
              padding: "20px",
              borderRadius: "5px",
              display: "block",
              textAlign: "center",
              height: "100%",
              marginTop: "40px",
              justifyContent: "center",
            }}
          >
            <Typography variant="h6">My Profile</Typography>
          </NavLink>
        </Grid>
        <Grid item xs={12} sm={6}>
          <NavLink
            to={ROUTES.HISTORY_PAGE}
            sx={{
              textDecoration: "none",
              color: "inherit",
              border: "1px solid white",
              padding: "20px",
              borderRadius: "5px",
              display: "block",
              textAlign: "center",
              height: "100%",
              marginTop: "40px",
            }}
          >
            <Typography variant="h6">My History</Typography>
          </NavLink>
        </Grid>
      </Grid>
    </Container>
  );
};

export default UserPage;
