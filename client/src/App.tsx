import { ThemeProvider } from "@mui/material/styles";
import { CssBaseline } from "@mui/material";
import { BrowserRouter } from "react-router-dom";
import Router from "./global/router/Router";
import Layout from "./global/layout/Layout";
import { theme } from "./global/styles/themes/themeModels";
import ToastifyStyleComponent from "./global/components/toastify/ToastifyStyleComponent";
import AuthProvider from "./auth/AuthProvider";
function App() {
  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <AuthProvider>
          <Layout>
            <Router />
          </Layout>
          <ToastifyStyleComponent />
        </AuthProvider>
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;
