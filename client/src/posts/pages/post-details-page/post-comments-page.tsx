import { useEffect } from "react";
import useSendRequestToServer from "../../hooks/useSendRequestToServer";
import Spinner from "../../../global/components/Spinner";
import CommentsCards from "../../components/comments/comments-cards";
import { PostWithComments } from "../../../global/types/types";
import { Grid } from "@mui/material";
import SentimentPieChart from "../../components/sentiment/SentimentPieChart";
import { toastHandler } from "../../../global/components/toastify/toastHandler";
interface Props {
  category: string | undefined;
  postId: string | undefined;
}

export default function PostCommentsPage({ category, postId }: Props) {
  const {
    data: postWithComments,
    pending,
    error,
    handleGetCommentsByPost,
  } = useSendRequestToServer<PostWithComments>();

  useEffect(() => {
    if (category && postId) {
      handleGetCommentsByPost({ category, postId });
    }
  }, []);
  useEffect(() => {
    if (error) toastHandler("could not fetch comments for this post", "error");
  }, [error]);
  if (pending)
    return (
      <div style={{ height: "400px" }}>
        <Spinner />
      </div>
    );
  if (postWithComments?.comments)
    return (
      <Grid style={{ display: "flex", flexDirection: "row" }}>
        <Grid display="flex" flexDirection="column">
          {<CommentsCards comments={postWithComments.comments} />}
        </Grid>
        <Grid>
          {postWithComments.comments[0]?.body ? (
            <SentimentPieChart comments={postWithComments?.comments} />
          ) : null}
        </Grid>
      </Grid>
    );
}
