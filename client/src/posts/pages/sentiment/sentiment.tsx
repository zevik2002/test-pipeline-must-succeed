import { Box, Grid, Typography } from "@mui/material";
import { FC } from "react";
import { SentimentEnum } from "../../../global/types/types";

const getSentimentColor = (sentiment: SentimentEnum): string => {
    if (sentiment === SentimentEnum.Positive) return "#12c511";
    if (sentiment === SentimentEnum.Negative) return "#dc5d41";
    return "";
};

const Sentiment: FC<{ sentiment: SentimentEnum }> = ({ sentiment }) => {
    return (
        <Grid item>
            <Box
                px={5}
                py={1}
                display="flex"
                alignItems="center"
                justifyContent="center"
                bgcolor={getSentimentColor(sentiment)}>
                <Typography
                    fontWeight={900}
                    sx={{
                        WebkitTextStrokeWidth: "1px",
                        WebkitTextStrokeColor: "#1e1e1e",
                    }}>
                    semantic analysis: {sentiment}
                </Typography>
            </Box>
        </Grid>
    );
};

export default Sentiment;
