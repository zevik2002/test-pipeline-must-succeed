import { useState } from "react";
import { Container } from "@mui/material";
import { titlesForSelectPages } from "../../helpers/titles";
import PageHeader from "../../../global/components/PageHeader";
import StepsFollower from "../../../global/components/stepsFollower/StepsFollower";
import SelectPostCategory from "../../components/post-selections/select-category/SelectPostCategory";
import SelectPostSubcategory from "../../components/post-selections/select-subcategory/SelectPostSubcategory";
import FilterPosts from "../../components/post-selections/filter-posts/FilterPosts";

const stepsLabels = titlesForSelectPages.map(label => label.title);

const SelectPostsPage = () => {
  const [stepIndex, setStepIndex] = useState(0);
  const [category, setCategory] = useState({ category: "", subCategory: "" });

  const titles = titlesForSelectPages[stepIndex];

  return (
    <Container>
      <PageHeader title={titles.title} subtitle={titles.subTitle} />
      <StepsFollower stepIndex={stepIndex} stepsArray={stepsLabels} />

      {stepIndex === 0 && <SelectPostCategory handelSteps={setStepIndex} onSetCategory={setCategory} />}
      {stepIndex === 1 && <SelectPostSubcategory onSetSubCategory={setCategory} handelSteps={setStepIndex} />}
      {stepIndex === 2 && <FilterPosts postCategory={category} handelSteps={setStepIndex} />}
    </Container>
  );
};

export default SelectPostsPage;
