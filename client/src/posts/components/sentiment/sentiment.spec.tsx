import { render, screen } from "@testing-library/react";
import Sentiment from "./sentiment";

test("should render Sentiment components", () => {
  render(<Sentiment sentiment={"NEGATIVE"} />);
  const sentimentElement = screen.getByText(/semantic analysis:\s*NEGATIVE/i);
  expect(sentimentElement).toBeDefined();
});
