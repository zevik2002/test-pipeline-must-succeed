import { useCallback } from "react";
import useReqInfoHandler, {
  ErrorType,
} from "../../global/hooks/useReqInfoHandler";
import {
  getHistoryData,
  getPost,
  getPostsByCategory,
  isCategoryExists,
  getCommentsByPost,
} from "../services/postApiService";
import { getErrorMessage } from "../../global/utils/algoMethods";
import { UserFormInput } from "../../global/types/types";
import { signUp } from "../../users/services/userApiService";
import { login } from "../../users/services/userApiService";
import useAxiosInterceptors from "../../global/hooks/useAxiosInterceptors";
import { useAuth } from "../../auth/AuthProvider";

type HandleGetPostType = {
  category: string;
  postId: string;
};

type handleSubmitLoginType = {
  email: string;
  password: string;
};

type ReturnUsePostType<R> = {
  data: R | null;
  pending: boolean;
  error: ErrorType;
  handelIsCategoryExists: (category: string) => Promise<void>;
  handelGetPostsByCategory: (
    category: string,
    subCategory: string
  ) => Promise<void>;
  handleGetPost: ({ category, postId }: HandleGetPostType) => Promise<void>;
  handelSignUp: (newUser: UserFormInput) => Promise<void>;
  handleSubmitLogin: (args: handleSubmitLoginType) => Promise<void>;
  handleGetCommentsByPost: ({
    category,
    postId,
  }: HandleGetPostType) => Promise<void>;
  handleGetUserHistory: (token: string) => Promise<void>;
};

const useSendRequestToServer = <T>(): ReturnUsePostType<T> => {
  useAxiosInterceptors();
  const {
    updateReqInfo,
    value: { data, error, pending },
  } = useReqInfoHandler<T>();
  const { setToken } = useAuth();

  const sendRequest = async (requestFunction: () => Promise<unknown>) => {
    updateReqInfo(true, null, null);
    requestFunction()
      .then((data) => updateReqInfo(false, null, data as T))
      .catch((error) => updateReqInfo(false, getErrorMessage(error), null));
  };

  const handelIsCategoryExists = useCallback(
    async (category: string) => sendRequest(() => isCategoryExists(category)),
    []
  );

  const handleGetPost = useCallback(
    async ({ category, postId }: HandleGetPostType) =>
      sendRequest(() => getPost(category, postId)),
    []
  );

  const handelGetPostsByCategory = useCallback(
    async (category: string, subCategory: string) =>
      sendRequest(() => getPostsByCategory(category, subCategory)),
    []
  );

  const handelSignUp = useCallback(
    async (newUser: UserFormInput) => sendRequest(() => signUp(newUser)),
    []
  );

  const handleGetUserHistory = useCallback(
    async () => sendRequest(() => getHistoryData()),
    []
  );

  const handleSubmitLogin = useCallback(async (data: handleSubmitLoginType) => {
    try {
      updateReqInfo(true, null, null);
      const token = await login(data.email, data.password);

      setToken(token);
      updateReqInfo(false, null, null);
    } catch (error) {
      updateReqInfo(false, error as string, null);
    }
  }, []);

  const handleGetCommentsByPost = useCallback(
    async ({ category, postId }: HandleGetPostType) =>
      sendRequest(() => getCommentsByPost(category, postId)),
    []
  );

  return {
    data,
    pending,
    error,
    handelIsCategoryExists,
    handelGetPostsByCategory,
    handleGetPost,
    handelSignUp,
    handleSubmitLogin,
    handleGetCommentsByPost,
    handleGetUserHistory,
  };
};

export default useSendRequestToServer;
