import axios from "axios";

const BASE_URL = import.meta.env.VITE_BASE_URL || "http://localhost:8080";

export const getPost = async (category: string, postId: string) => {
  try {
    const { data: post } = await axios.get(
      `${BASE_URL}/posts/details/${category}/${postId}`
    );

    return post;
  } catch (error) {
    return Promise.reject(error);
  }
};

export const isCategoryExists = async (category: string) => {
  const { data: isExists } = await axios.get(
    `${BASE_URL}/posts/categories/${category}/check`
  );
  return isExists;
};

export const getPostsByCategory = async (
  category: string,
  subCategory: string
) => {
  const { data: categories } = await axios.get(
    `${BASE_URL}/posts/${category}/${subCategory}`
  );
  return categories;
};

export async function getHistoryData() {
  const { data: historyData } = await axios.get(`${BASE_URL}/users/history`);  
  return historyData;
}
export const getCommentsByPost = async (category: string, postId: string) => {
  const { data: post } = await axios.get(
    `${BASE_URL}/posts/comments/${category}/${postId}`
  );
  return post;
};
