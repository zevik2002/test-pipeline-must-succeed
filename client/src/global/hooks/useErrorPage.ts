import { useAtom } from "jotai";
import { errorPagePropsAtom } from "../../states/atoms";
import { ErrorPageProps } from "../types/types";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";

export const useErrorPage = (props: ErrorPageProps | null) => {
  const navigate = useNavigate();
  const [, setErrorPageProps] = useAtom(errorPagePropsAtom);
  useEffect(() => {
    if (props) {
      const { status, message } = props;
      setErrorPageProps({ status, message: message ? message : undefined });
      navigate(`/error`);
    } else return;
  }, [props]);
};