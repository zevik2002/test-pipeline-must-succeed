import { useState } from "react";

export type ErrorType = null | string;

type RequestStatusType<T> = (
  pending: boolean,
  errorMessage: ErrorType,
  data: T | null
) => void;

type ReturnType<T> = {
  value: {
    error: ErrorType;
    pending: boolean;
    data: T | null;
  };
  updateReqInfo: RequestStatusType<T>;
};

const useReqInfoHandler = <T>(): ReturnType<T> => {
  const [pending, setPending] = useState(false);
  const [error, setError] = useState<ErrorType>(null);
  const [data, setData] = useState<T | null>(null);

  const updateReqInfo = (
    pending: boolean,
    errorMessage: ErrorType,
    data: T | null
  ) => {
    setPending(pending);
    if (errorMessage) setError(errorMessage);
    if (data) setData(data);
  };

  const value = { error, pending, data }
  return { value, updateReqInfo };
};

export default useReqInfoHandler;
