import { CardMedia, IconButton } from "@mui/material";
import { Box } from "@mui/system";

const LogoIcon = () => {
  return (
    <Box>
      <IconButton edge="start" color="inherit" aria-label="menu">
        <CardMedia
          style={{
            width: "30px",
            height: "auto",
          }}
          component="img"
          image="../../../../../../images/reddit-logo.png"
          alt="Logo"
        />
      </IconButton>
    </Box>
  );
};

export default LogoIcon;
