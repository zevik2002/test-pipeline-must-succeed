import { render } from "@testing-library/react";
import Logo from "./Logo";

describe("Logo", () => {
  it("renders the logo text", () => {
    const { getByText } = render(<Logo />);
    expect(getByText("Reddit Emotion")).toBeTruthy();
  });

  it("applies custom styles when sx prop is provided", () => {
    const sx = { color: "red" };
    const { getByText } = render(<Logo sx={sx} />);
    expect(getByText("Reddit Emotion"));
  });
});
