import { render, screen } from "@testing-library/react";
import Main from "./Main";

describe("Main testing", () => {
  it("renders children", () => {
    const children = "Test";
    render(<Main>{children}</Main>);
    expect(screen.getByText(children)).toBeTruthy();
  });
});
