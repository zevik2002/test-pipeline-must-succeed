import { ReactNode } from "react";
import { Paper } from "@mui/material";

type props = {
  children: ReactNode;
};

const Main: React.FC<props> = ({ children }) => {
  return <Paper sx={{ flexGrow: 1 }}>{children}</Paper>;
};

export default Main;
