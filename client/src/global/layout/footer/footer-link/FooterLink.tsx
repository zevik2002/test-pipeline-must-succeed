import { Grid, Typography } from "@mui/material";
import { CSSProperties, FC, ReactNode } from "react";

type FooterLinkProps = {
  children: ReactNode;
  sx?: CSSProperties;
};

const FooterLink: FC<FooterLinkProps> = ({ children, sx }) => {
  return (
    <Grid item>
      <Typography sx={sx}>{children}</Typography>
    </Grid>
  );
};

export default FooterLink;
