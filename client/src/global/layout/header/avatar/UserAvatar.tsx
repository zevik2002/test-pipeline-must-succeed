import { Avatar } from "@mui/material";
import ROUTES from "../../../router/routes-model";
import NavLink from "../../../router/components/NavLink";
import { theme } from "../../../styles/themes/themeModels";
import { useAuth } from "../../../../auth/AuthProvider";

export default function UserAvatar() {
  const { user } = useAuth();

  const randomBackgroundColor = () => {
    const blue = theme.palette.info.main;
    const green = theme.palette.success.main;
    const orange = theme.palette.warning.light;
    const yellow = theme.palette.secondary.main;
    const colors = [blue, green, orange, yellow];
    const randomIndex = Math.floor(Math.random() * colors.length);
    return colors[randomIndex];
  };

  const getInitials = (firstName: string, lastName: string) => {
    const initForFirstName = firstName.charAt(0) || "";
    const initForLastName = lastName.charAt(0) || "";
    return (initForFirstName + initForLastName).toUpperCase();
  };

  const initials = () => {
    if (user) {
      const { name } = user;
      const { first, last } = JSON.parse(name as unknown as string);
      return getInitials(first, last);
    }
    return "";
  };

  return (
    <NavLink to={ROUTES.PROFILE} sx={{}}>
      {user && (
        <Avatar
          sx={{
            bgcolor: randomBackgroundColor,
            height: "32px",
            width: "32px",
            marginTop: "2px",
            transition: "box-shadow 0.3s",
            "&:hover": {
              boxShadow: "0 0 8px 2px rgba(0,0,0,0.5)",
            },
          }}
        >
          {initials()}
        </Avatar>
      )}
    </NavLink>
  );
}
