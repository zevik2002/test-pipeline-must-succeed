import { Typography } from "@mui/material";

import { Box } from "@mui/system";

const Logo = () => {
  return (
    <Box sx={{ display: { xs: "none", md: "flex" }, alignItems: "center" }}>
      <Typography>Reddit Emotion</Typography>
    </Box>
  );
};

export default Logo;
