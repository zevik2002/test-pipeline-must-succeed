import { Grid } from "@mui/material";
import Footer from "./footer/Footer";
import Header from "./header/Header";
import { ReactNode } from "react";
import Main from "./main/Main";

type Props = {
  children: ReactNode;
};

const Layout: React.FC<Props> = ({ children }) => {
  return (
    <Grid container direction="column" minHeight="100vh">
      <Header />
      <Main>{children}</Main>
      <Footer />
    </Grid>
  );
};

export default Layout;
