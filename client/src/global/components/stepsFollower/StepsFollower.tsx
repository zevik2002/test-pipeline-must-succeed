import { Box, Step, StepLabel, Stepper } from "@mui/material";
import { FC } from "react";

interface StepsFollowerProps {
  stepIndex: number;
  stepsArray: string[];
}

const StepsFollower: FC<StepsFollowerProps> = ({ stepIndex, stepsArray }) => {
  return (
    <Box my={4}>
      <Stepper activeStep={stepIndex} alternativeLabel>
        {stepsArray.map((label, index) => (
          <Step key={index}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
    </Box>
  );
};

export default StepsFollower;
