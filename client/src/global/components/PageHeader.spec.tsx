import { render, screen } from "@testing-library/react";
import PageHeader from "../components/PageHeader";

describe("PageHeader component", () => {
  test("renders PageHeader component with title", () => {
    const titleText = "Reddit Emotions Site";

    render(<PageHeader title={titleText} subtitle="" />);

    const titleElement = screen.getByText(titleText) as HTMLElement;
    expect(titleElement).toBeTruthy();
  });

  test("renders PageHeader component with subtitle", () => {
    const subtitleText =
      "Reddit is home to thousands of communities, endless conversation, and authentic human connection.";

    render(<PageHeader title="" subtitle={subtitleText} />);

    const subtitleElement = screen.getByText(subtitleText) as HTMLElement;
    expect(subtitleElement).toBeTruthy();
  });
});
