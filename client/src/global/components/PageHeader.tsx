import { Typography, Grid } from "@mui/material";
import {
  capitalizeFirstLetterOfEachWord,
  makeFirstLetterCapital,
} from "../utils/algoMethods";

interface Props {
  title: string;
  subtitle?: string;
}

const PageHeader: React.FC<Props> = ({ title, subtitle }) => {
  return (
    <Grid justifyContent="center" container p={2}>
      <Grid item xs={12} sm={10} md={8} lg={6}>
        <Typography
          textAlign="center"
          variant="h4"
          component="h1"
          color="white"
          marginTop="3vh"
          sx={{ mb: 2 }}
        >
          {capitalizeFirstLetterOfEachWord(title)} 
        </Typography>
        <Typography variant="h6" component="h2" color="primary">
          </Typography>

        {subtitle && (
          <Typography variant="h6" component="h2" color="primary">
            {makeFirstLetterCapital(subtitle)}
          </Typography>
        )}
      </Grid>
    </Grid>
  );
};

export default PageHeader;
