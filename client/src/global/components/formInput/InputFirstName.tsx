import { Box, TextField, Typography } from "@mui/material"
import { FromProps } from "./propsType"
import { theme } from "../../styles/themes/themeModels"

const InputName = (props: FromProps) => {
    return (
        <Box>
            <TextField
            margin="normal"
            fullWidth
            variant="outlined"
            label= "first Name"
            type="string"
            {...props.register("name.first", { required: "first name is required" ,
            maxLength: { value: 10, message: "first name must be at most 15 characters" },
            minLength: { value: 2, message: "first name must be at least 2 characters" } })}
            onBlur={() => props.trigger("name.first")}
          />
          {props.formState.errors.name?.first && <Typography color={theme.palette.error.light}>{props.formState.errors.name.first.message}</Typography>}
        </Box>
    )
}
export default InputName;