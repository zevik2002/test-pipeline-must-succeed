import { Zoom } from "react-toastify";

export const toastStyles = {
  infoStyle: {
    position: "top-right" as const,
  },
  errorStyle: {
    autoClose: 8000 as const,
    pauseOnHover: false as const,
  },
  successStyle: {
    transition: Zoom,
    pauseOnHover: false as const,
  },
  warnStyle: {
    autoClose: 8000 as const,
  },
  defaultStyle: {
    position: "bottom-center" as const,
  },
};
