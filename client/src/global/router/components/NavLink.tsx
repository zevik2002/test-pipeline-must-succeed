import { CSSProperties, FC, ReactNode } from "react";
import { NavLink as RouterNavLink } from "react-router-dom";

type NavLinkProps = {
  to: string;
  children: ReactNode;
  color?: string;
  sx?: CSSProperties;
};

const NavLink: FC<NavLinkProps> = ({ to, color = "white", sx, children }) => {
  return (
    <RouterNavLink style={{ textDecoration: "none", color, ...sx }} to={to}>
      {children}
    </RouterNavLink>
  );
};

export default NavLink;
