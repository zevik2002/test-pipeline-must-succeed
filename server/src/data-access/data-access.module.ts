import { Module } from '@nestjs/common';
import PostsDal from 'src/posts/post.dal';


@Module({
  providers: [PostsDal],
  exports: [PostsDal]
})
export class DataAccessModule {}
