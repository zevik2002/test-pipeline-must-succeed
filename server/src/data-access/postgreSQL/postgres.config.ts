import { TypeOrmModuleAsyncOptions } from '@nestjs/typeorm';
import { PostEntity } from 'src/posts/entity/post.entity';
import { UserEntity } from 'src/users/entity/user.entity';

const TypeOrmOptions: TypeOrmModuleAsyncOptions = {
  useFactory: async () => ({
    type: 'postgres',
    host: process.env.PG_HOST,
    port: parseInt(process.env.PG_PORT),
    username: process.env.PG_USER,
    password: String(process.env.PG_PASSWORD),
    database: process.env.PG_DB,
    entities: [UserEntity, PostEntity],
    synchronize: true,
    logging: false ,
  }),
};

export default TypeOrmOptions;
