import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { redditTokenHandler } from './data-access/reddit/redditTokenHandler';
import logger from './logger/GeneralLogger';
import { config } from 'dotenv';
import corsOptions from './cors/cors-options';
config();

const PORT = process.env.PORT || 8080;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({ origin: '*' });
  // app.enableCors(corsOptions);

  await app.listen(PORT, () => {
    redditTokenHandler();
    logger('App Listen', `app listens on http://localhost:${PORT}`);
  });
}
bootstrap();

