export const capitalizeAllFirstLetters = (string: string) => {
  const regex = /\b\w/g;
  return string.toLowerCase().replace(regex, (letter) => {
    return letter.toUpperCase();
  });
};
