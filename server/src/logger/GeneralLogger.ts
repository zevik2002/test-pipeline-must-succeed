import { Logger } from '@nestjs/common';
import { capitalizeAllFirstLetters } from 'src/global/utils/algo';

export type LoggerColorsTypes = 'red' | 'green' | 'yellow';

const logger = (
  label: string,
  log: string,
  color: LoggerColorsTypes = 'green',
) => {
  const newLogger = new Logger(capitalizeAllFirstLetters(label));
  if (color === 'green') newLogger.log(log);
  if (color === 'red') newLogger.error(log);
  if (color === 'yellow') newLogger.warn(log);
  return newLogger;
};

export default logger;
