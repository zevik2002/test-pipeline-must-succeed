import {
  Body,
  Controller,
  Get,
  HttpStatus,
  NotFoundException,
  Param,
  Post,
  Req,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { PostsService } from './posts.service';
import { IsLoginGuard } from 'src/auth/guards/is-login/is-login.guard';
import { UserPayloadInterface } from 'src/auth/interfaces/TokenInterface';
import { CustomHttpException } from 'src/error-handling/custom-error-handl';

@Controller('posts')
export class PostsController {
  constructor(private postService: PostsService) {}

  @Get('comments/:category/:id')
  getPostAndHisComments(
    @Param('category') category: string,
    @Param('id') id: string,
  ) {
    try {
      return this.postService.getPostAndHisComments(category, id);
    } catch (error) {
      throw new CustomHttpException(
        'could not fetch post comments',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get('/categories')
  getCategories() {
    return this.postService.getCategoriesService();
  }

  @Get('/categories/:category/check')
  async checkCategory(@Param('category') categoryName: string) {
    try {
      const isCategoryExists =
        await this.postService.checkCategory(categoryName);
      return isCategoryExists;
    } catch (error) {
      throw new NotFoundException('Category not found!');
    }
  }

  @Get(':category/:subCategory')
  getPostsByCategory(
    @Param('category') category: string,
    @Param('subCategory') subCategory: string,
  ) {
    return this.postService.getPostsByCategoryService(category, subCategory);
  }

  @UseGuards(IsLoginGuard)
  @Get('details/:category/:postId')
  getPost(
    @Param('category') category: string,
    @Param('postId') postId: string,
    @Req() req: UserPayloadInterface,
  ) {
    const userId = req.user._id;
    return this.postService.getByIdService(category, postId, userId);
  }
}
