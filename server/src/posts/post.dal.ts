import { Injectable, NotFoundException } from '@nestjs/common';
import { fetchRedditAPI } from '../data-access/reddit/redditRequest';
import { Categories, PostsByCategory } from '../types';
import normalizePost from './helpers/normalizePost';
import RedditComment from './interfaces/RedditComment';

@Injectable()
class PostsDal {
  async getById(category: string, id: string) {
    try {
      const response = await fetchRedditAPI(`/r/${category}/comments/${id}`);
      const redditPost = response[0].data.children[0].data;
      const postNormalized = normalizePost(redditPost);
      return postNormalized;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async getPostsByCategory(category: string, subCategory: string) {
    try {
      const postsByCategoryDB = await fetchRedditAPI(
        `/r/${category}/${subCategory}?limit=100`,
      );
      const posts = postsByCategoryDB.data.children.map(
        (post: PostsByCategory) => normalizePost(post.data),
      );
      return posts;
    } catch (error) {
      Promise.reject(error);
    }
  }

  async getCategories() {
    try {
      const categoriesDB = await fetchRedditAPI(
        '/subreddits/popular?limit=1000',
      );
      return categoriesDB.data.children.map((category: Categories) => {
        const subredditData = category.data;
        return {
          title: subredditData.title,
          category_url: subredditData.display_name,
        };
      });
    } catch (error) {
      Promise.reject(error);
    }
  }

  async getPostAndHisComments(category: string, id: string) {
    const response = await fetchRedditAPI(`/r/${category}/comments/${id}`);
    const redditPost = response[0].data.children[0].data;

    const post = normalizePost(redditPost);

    const redditComments: RedditComment[] = response[1].data.children;

    const comments = [];
    redditComments.forEach(({ data }) =>
      comments.push(this.getCommentTree(data)),
    );

    return { post, comments };
  }

  async checkCategory(categoryName: string) {
    try {
      const category = await fetchRedditAPI(`/r/${categoryName}/about.json`);
      if (!category) throw new Error('category not found!');
      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  getCommentTree(object) {
    if (
      typeof object.replies === 'object' &&
      object.replies.data.children[0].kind === 't1'
    ) {
      const replies = object.replies.data.children;
      const arrOfComments = [];
      replies.forEach((element) => {
        arrOfComments.push(this.getCommentTree(element.data));
      });
      return {
        body: object.body,
        author: object.author,
        created: object.created,
        replies: arrOfComments,
      };
    } else {
      return {
        body: object.body,
        author: object.author,
        created: object.created,
      };
    }
  }
}
export default PostsDal;
