import { PostEntity } from '../entity/post.entity';

const normalizePost = (post): Omit<PostEntity, 'sentiment'> => {
  return {
    _id: post.id,
    title: post.title,
    img: post.url_overridden_by_dest || post.thumbnail,
    category: post.subreddit,
    selftext: post.selftext,
    author: post.author,
    created: new Date(post.created * 1000),
  };
};

export default normalizePost;
