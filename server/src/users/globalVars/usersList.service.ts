import { Injectable } from '@nestjs/common';
import { SignUpDto } from '../dto/signUp.dto';

@Injectable()
export class UsersListService {
  list: SignUpDto[] = [];
}
