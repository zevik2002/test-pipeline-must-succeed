import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { NameDto, UserRequestDto } from '../dto/user.dto';
import { PostEntity } from 'src/posts/entity/post.entity';

@Entity()
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar' })
  name: NameDto;

  @Column({ type: 'varchar', unique: true })
  email: UserRequestDto['email'];

  @Column({ type: 'varchar' })
  password: UserRequestDto['password'];

  @Column({ type: 'boolean', default: false })
  isAdmin: boolean;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
    onUpdate: 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @ManyToMany(() => PostEntity)
  @JoinTable()
  history: PostEntity[];
}

export class UserFullNameEntity {
  @PrimaryGeneratedColumn()
  _id: number;

  @Column({ type: 'varchar', length: 50 })
  first: NameDto['first'];

  @Column({ type: 'varchar', length: 50 })
  last: NameDto['last'];
}
