import { Type } from 'class-transformer';
import {
  IsAlphanumeric,
  IsArray,
  IsBoolean,
  IsDate,
  IsNotEmpty,
  IsString,
  Matches,
  MinLength,
  ValidateNested,
} from 'class-validator';
import { emailRegex, passwordRegex } from 'src/global/utils/regex';

export class NameDto {
  @IsString()
  @MinLength(2, { message: 'firstName must be at least 2 characters' })
  @IsAlphanumeric()
  first: string;

  @IsString()
  @MinLength(2, { message: 'firstName must be at least 2 characters' })
  @IsAlphanumeric()
  last: string;
}
export class UserRequestDto {
  
  @ValidateNested()
  @Type(() => NameDto)
  name: NameDto;
  
  @Matches(emailRegex, { message: 'email must be a valid email' })
  email: string;

  @Matches(passwordRegex, { message: 'password must be a valid email' })
  password: string;
}

export class UserResponseDto extends UserRequestDto {
  @IsNotEmpty()
  id: number;

  @IsBoolean()
  isAdmin: boolean;

  @IsDate()
  createdAt: Date;

  @IsDate()
  updatedAt: Date;

  @IsArray()
  myPostHistory: string[];
}
