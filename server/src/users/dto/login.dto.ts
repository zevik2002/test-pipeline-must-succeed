import { UserRequestDto } from './user.dto';

export class LoginDto {
  email: UserRequestDto['email'];
  password: UserRequestDto['password'];
}
