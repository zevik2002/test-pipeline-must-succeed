import { Test, TestingModule } from '@nestjs/testing';
import { SentimentAnalysisService } from './sentiment-analysis.service';

describe('SentimentAnalysisService', () => {
  let service: SentimentAnalysisService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SentimentAnalysisService],
    }).compile();

    service = module.get<SentimentAnalysisService>(SentimentAnalysisService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  it('test sentiment analysis with long text', async () => {
    const post = {
      selftext:
        "In the intricate and delicate tapestry of existence, there exists a profound beauty woven into the fabric of everyday life where the subtle nuances and overlooked details serve as the threads that bind us to the essence of our being. Each moment, seemingly mundane, holds within it the potential to captivate the depths of the soul and evoke a sense of wonderment that transcends the boundaries of time and space. Consider, for a moment, the gentle unfolding of dawn as it breaks across the horizon, casting hues of gold and pink upon the canvas of the sky. In this symphony of light and color, there lies a silent serenade, a whispered secret of tranquility that resonates with those who pause to witness its splendor. It is in these early hours, when the world is cloaked in the soft embrace of morning, that one can find solace in the simple act of being, of existing within the embrace of nature's embrace. And yet, amidst the grandeur of the natural world, there exists a beauty equally profound in the rituals of human connection, in the shared moments of laughter and love that punctuate the rhythm of our lives. Consider the warmth of a shared meal among loved ones, where the clinking of utensils mingles with the echoes of joy and camaraderie, creating a symphony of its own. In these moments of communion, we find not only sustenance for the body but nourishment for the soul, as we are reminded of the deep-seated longing for connection that resides within us all. And what of the smaller, quieter moments that often pass unnoticed in the hustle and bustle of daily life? The gentle caress of a breeze against sun-kissed skin, the familiar scent of home after a long journey – these too hold within them the essence of beauty, if only we take the time to pause and appreciate their significance. It is in the imperfections of life, the cracks in the facade of perfection, that true beauty is revealed. Like the weathered branches of an ancient tree or the rough-hewn edges of a well-loved book, our flaws serve as testaments to our humanity, reminders that it is through our struggles and imperfections that we find strength and resilience. And so, it is through the lens of mindfulness that we learn to see the world with fresh eyes, to appreciate the beauty that surrounds us in its myriad forms. In the quiet moments of reflection, where time seems to stand still and the noise of the world fades into the background, we find ourselves immersed in a state of gratitude and awe for the sheer miracle of existence. It is here, in the depths of our own consciousness, that we come to understand the true meaning of life – not as a series of tasks to be completed or goals to be achieved, but as a journey of discovery and wonder, an ever-unfolding tapestry of experience and emotion. And so, let us embrace each moment with open hearts and open minds, embracing the unpredictability of life with courage and grace. For it is in our willingness to surrender to the flow of life, to dance freely amidst its ever-changing melodies, that we find true joy and fulfillment",
    };
    const result = await service.getAnalysis(post.selftext);
    expect(result).toEqual('POSITIVE');
  });
});
