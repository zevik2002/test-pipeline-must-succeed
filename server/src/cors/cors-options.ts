import { config } from 'dotenv';

config();

const WHITE_LIST = process.env.WHITE_LIST || 'http://localhost:5173';

const corsOptions = (req, callback) => {
  const isExist = !!WHITE_LIST.includes(req.headers.origin);
  if (!isExist)
    return callback(new Error('CORS'), {
      origin: false,
    });
  callback(null, { origin: true });
};

export default corsOptions;
//BBSARA
//Yair