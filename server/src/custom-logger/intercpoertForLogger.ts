import {
    Injectable,
    NestInterceptor,
    ExecutionContext,
    CallHandler,
    HttpStatus,
  } from '@nestjs/common';
  import { Observable } from 'rxjs';
  import { tap } from 'rxjs/operators';
  import { CustomLoggerService } from './customLogger.service';
  
  @Injectable()
  export class LoggingInterceptor implements NestInterceptor {
    constructor(private logger: CustomLoggerService) {}
  
    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
      const req = context.switchToHttp().getRequest();
      const res = context.switchToHttp().getResponse();
  
      const now = Date.now();
      return next
        .handle()
        .pipe(
          tap(() => {
            const after = Date.now() - now;
            const { method, originalUrl } = req;
            const { statusCode } = res;
            const statusColor = statusCode >= HttpStatus.BAD_REQUEST ? 'red' : 'green'; // Example color coding
            this.logger.log(
              `${method} ${originalUrl} ${statusCode} ${statusColor === 'red' ? 'ERROR' : 'SUCCESS'} ${after}ms`,
            );
          }),
        );
    }
  }
  